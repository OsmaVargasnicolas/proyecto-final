function cargarMapa() {
    let mapa = L.map('divMapa', { center: [4.709186339035933, -74.22400832176208], zoom: 16 });

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

    let unMarcador = L.marker([4.709322, -74.224027]);
    unMarcador.addTo(mapa);

    let poligono= L.polygon([
        [4.7093253430566655,-74.22589659690857],
        [4.708320236434825,-74.22724843025208],
        [4.705914390219061, -74.22488808631897],
        [4.705796770857494, -74.22400832176208],
        [4.707561059193925,  -74.22189474105835],
        [4.710822307673075,-74.22378301620483],
        [4.7093253430566655, -74.22589659690857],
      ])
      poligono.addTo(mapa);

}


function mapaCC ()
{
    let mapa = L.map('divMapaCC', { center: [4.713367140123769, -74.22181963920593], zoom: 16 });

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

    let unMarcador = L.marker([4.713367140123769,-74.22181963920593]);
    unMarcador.addTo(mapa);
}

function mapaParque ()
{
    let mapa = L.map('divMapaParque', { center: [4.707988764783643,-74.22336459159851], zoom: 16 });

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

    let unMarcador = L.marker([4.707988764783643,-74.22336459159851]);
    unMarcador.addTo(mapa);
}

function localSto () 
{
    let nombre=getElementById("nombre1").value;
    localStorage.getItem("nombre");
    let apellido=getElementById("apellido1").value;
    localStorage.getItem("apellido");
    let nacimiento=getElementById("fecha").value;
    localStorage.getItem("nacimiento");
    let correo=getElementById("correo1").value;
    localStorage.getItem("correo");
    let numero=getElementById("numero1").value;
    localStorage.getItem("numero");
}

var arreglo=[];
var arreglo2=[];
var i = 0;

function store(){
    //evento
    var inputEvent = document.getElementById("evento123");
    localStorage.setItem("evento123", inputEvent.value);
    var valorGuardado = localStorage.getItem("evento123");
    arreglo[i]=valorGuardado;

    //fecha
    var inputDate = document.getElementById("fecha123");
    localStorage.setItem("fecha123", inputDate.value);
    var fechaGuardada = localStorage.getItem("fecha123");
    arreglo2[i]=fechaGuardada;
    i=i+1;

    document.getElementById("evento1").innerHTML= JSON.stringify(arreglo);
    document.getElementById("fecha1").innerHTML = JSON.stringify(arreglo2);
}
